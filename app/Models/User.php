<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function categories() {
        return $this->belongsToMany(
            Category::class,
            'category_users',
            'user_id',
            'category_id',
            'id',
            'id'
        );
    }

    public function postedItems() {
        return $this->hasMany(
            Item::class,
            'user_id',
            'id'
        );
    }

    public function foundItems() {
        return $this->belongsToMany(
            Item::class,
            'item_users',
            'user_id',
            'item_id',
            'id'
        );
    }
}
