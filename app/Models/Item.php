<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public function beforeImages() {
        return $this->hasMany(ItemImage::class, 'item_id', 'id')
            ->where('is_before', 1);
    }

    public function images() {
        return $this->hasMany(ItemImage::class, 'item_id', 'id')
            ->orderBy('is_before', 'desc');
    }

    public function categories() {
        return $this->belongsToMany(
            Category::class,
            'category_items',
            'item_id',
            'category_id',
            'id',
            'id'
        );
    }
}
